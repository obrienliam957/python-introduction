# PICUP - Partnership for Integration of Computation into Undergraduate Physics

The exercises are from https://www.compadre.org/PICUP/exercises/

## Project Install

To install the dependencies of this project, run

`pip install -r requirements.txt`

When editing the project, you can update requirements.txt by running

`pip freeze > requirements.txt`
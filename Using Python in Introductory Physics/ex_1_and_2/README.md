# Exercise 1

## Pre-Requisites

I had to install matplotlib, scipy and numpy

`pip install numpy scipy matplotlib`

I also had to setup Python Interpreter:

- In Pycharm open settings `ctrl + alt + s`
- Project:<project_name>
- Project Interpreter
- Click the cog icon next to Interpreter selection
- Add...
- System Interpreter
- Select Python <version> which should have been installed from earlier
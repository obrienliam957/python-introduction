import numpy as np
import matplotlib.pyplot as mpl

totalTime = 10
dt = 0.1
N = totalTime / dt

t0 = 0
x0 = 1
v0 = 0

t = t0
x = x0
v = v0

time = np.array([t])
displacement = np.array([x])
velocity = np.array([v])

for i in range(int(N)):
    a = -x

    t = t + dt
    v = v + a * dt
    x = x + v * dt

    time = np.append(time, t)
    displacement = np.append(displacement, x)
    velocity = np.append(velocity, v)

mpl.figure(1)

expectedDisplacementSolution = np.cos(time)

mpl.plot(time, displacement, 'ro')
mpl.plot(time, expectedDisplacementSolution, 'b-')

mpl.title("Simple Harmonic Oscillator")
mpl.xlabel("Time (t)")
mpl.ylabel("Displacement (m)")

mpl.figure(2)

expectedVelocitySolution = - np.sin(time)

mpl.plot(time, velocity, 'ro')
mpl.plot(time, expectedVelocitySolution, 'b-')

mpl.title("Simple Harmonic Oscillator")
mpl.xlabel("Time (t)")
mpl.ylabel("Velocity (ms-1)")

mpl.show()

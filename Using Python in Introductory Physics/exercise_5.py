import numpy as np
import matplotlib.pyplot as mpl

# For free falling ball, a=-g

a = -9.8
dt = 0.1
N = 100

x0 = 25
v0 = 50

t = 0
x = x0
v = v0

time = np.array([0])
height = np.array([x0])
velocity = np.array([v0])

for i in range(N):
    t = t + dt
    x = x + v * dt
    v = v + a * dt

    time = np.append(time, t)
    height = np.append(height, x)
    velocity = np.append(velocity, v)

mpl.figure(1)

mpl.plot(time, height, 'ro')
mpl.plot(time, x0 + 0.5 * a * time**2 + v0 * time, 'b-')

mpl.xlabel("Time (s)")
mpl.ylabel("Height (m)")
mpl.title("Trajectory of ball thrown in the air")

mpl.figure(2)

mpl.plot(time, velocity, '-ro')

mpl.xlabel("Time (s)")
mpl.ylabel("Velocity (ms**-1)")
mpl.title("Trajectory of ball thrown in the air")

mpl.show()
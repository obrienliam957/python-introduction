# Introduction to Learning Physics with Python

The exercises are from https://www.compadre.org/picup/exercises/exercise.cfm?A=Python

## Python Install

I am using (on a Windows 10 Desktop):

Pycharm: https://www.jetbrains.com/pycharm/

Python: https://www.python.org/downloads/

This will install the IDE and Python, as well as pip and other useful tools.

import matplotlib.pyplot as mpl
import numpy as np

turns = list(range(0, 11))
position = [0, -2, -11, -15, -24, -32, -40, -50, -52, -62, -65]

# Expect best fit to be linear
coefficient = np.polyfit(turns, position, 1)

# Use poly1d to return a function which takes in x and returns an estimate for y
poly1d_fn = np.poly1d(coefficient)

print("Line of best fit: y = {}x + {}".format(coefficient[0], coefficient[1]))

mpl.plot(turns, position, '-ro', label="experimental values")
mpl.plot(turns, poly1d_fn(turns), 'b-', label="best fit")

mpl.title("Plot of position of magnet with each incremental turn")
mpl.xlabel("Number of turns")
mpl.ylabel("Magnet position (degrees)")

mpl.legend(loc="upper right")

mpl.show()

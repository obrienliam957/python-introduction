import numpy as np
from targetdistance import TargetDistance
from display import Display
from scipy.optimize import brentq

plotDisplay = Display(311, "Calculating roots of complex algebraic functions")

xlabel = "Theta (radians)"
ylabel = "F(theta)"

thetaRange = np.linspace(0, 2*np.pi, 1000)
projectileHeight = 1.2
initialVelocity = 4.8

targetDistance1 = 1
calculateDistance1 = TargetDistance(targetDistance1, projectileHeight, initialVelocity)

plotDisplay.printPlot(thetaRange, calculateDistance1.calculateF(thetaRange), xlabel, ylabel, "R=1")

targetDistance2 = 5
calculateDistance2 = TargetDistance(targetDistance2, projectileHeight, initialVelocity)

plotDisplay.printPlot(thetaRange, calculateDistance2.calculateF(thetaRange), xlabel, ylabel, "R=5")

targetDistance3 = 5
projectileHeight3 = 3.2
initialVelocity3 = 6.8
calculateDistance3 = TargetDistance(targetDistance3, projectileHeight3, initialVelocity3)

plotDisplay.printPlot(thetaRange, calculateDistance1.calculateF(thetaRange), xlabel, ylabel, "R=5, h=3.2, v0=6.8")

answer1 = brentq(calculateDistance1.calculateF, 0.5, 1.5)
answer3 = brentq(calculateDistance3.calculateF, 0.5, 1.5)

print("Solution for graph1: {}".format(answer1))
print("No solution for graph 2")
print("Solution for graph3: {}".format(answer3))

plotDisplay.show()

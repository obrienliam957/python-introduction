import matplotlib.pyplot as mpl
import numpy as np

subplotNumber = 211


def printplot(x, y, graphXlabel, graphYlabel):
    global subplotNumber
    mpl.subplot(subplotNumber)
    subplotNumber += 1

    mpl.plot(x, y, '-b')

    mpl.xlabel(graphXlabel)
    mpl.ylabel(graphYlabel)


a = 9.81

t = np.linspace(0, 10, 1000)
y = 0.5 * a * t ** 2
v = a * t

mpl.figure()
mpl.suptitle("Graph showing free falling object")

printplot(t, y, "Time (s)", "Displacement (m)")
printplot(t, v, "Time (s)", "Velocity (ms-1)")

mpl.show()

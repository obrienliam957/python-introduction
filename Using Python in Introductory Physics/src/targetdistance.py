import numpy as np

gravconstant = 9.8


class TargetDistance:

    def __init__(self, targetdistance, projectileheight, initialvelocity):
        self.__targetdistance = targetdistance
        self.__projectileheight = projectileheight
        self.__initialvelocity = initialvelocity

    def calculateF(self, theta):
        root = np.sqrt(self.__initialvelocity**2 * np.sin(theta)**2 + 2 * gravconstant * self.__projectileheight)
        splitcalc = self.__initialvelocity * np.sin(theta) + root
        calculateddistance = (self.__initialvelocity * np.cos(theta) / gravconstant) * splitcalc

        return calculateddistance - self.__targetdistance

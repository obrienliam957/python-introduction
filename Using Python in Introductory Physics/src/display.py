import matplotlib.pyplot as mpl


class Display:

    def __init__(self, subplotNumber, graphTitle):
        self.__subplotNumber = subplotNumber
        mpl.figure()
        mpl.suptitle(graphTitle)

    def printPlot(self, x, y, graphXlabel, graphYlabel, title):
        self.printSubPlot(x, y, graphXlabel, graphYlabel)
        mpl.title(title)

    def printSubPlot(self, x, y, graphXlabel, graphYlabel):
        mpl.subplot(self.__subplotNumber)
        self.__subplotNumber += 1

        mpl.plot(x, y, '-b')

        mpl.xlabel(graphXlabel)
        mpl.ylabel(graphYlabel)

    def show(self):
        mpl.show()
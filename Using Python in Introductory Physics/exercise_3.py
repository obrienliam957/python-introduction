import numpy as np
import matplotlib.pyplot as mpl
from targetdistance import TargetDistance

targetdistance = 2.5
projectileheight = 1.2
initialvelocity = 4.8

calculateDistance = TargetDistance(targetdistance, projectileheight, initialvelocity)

thetaRange = np.linspace(0, 2*np.pi, 1000)
calculated = calculateDistance.calculateF(thetaRange)

mpl.plot(thetaRange, calculated, '-b')

mpl.title("Plot of F(theta) to determine angle theta to hit target at distance R away")
mpl.xlabel("Theta (radians)")
mpl.ylabel("F(theta)")

mpl.show()


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
plt.style.use('dark_background')

figure = plt.figure()

axes = plt.axes(xlim=(-50, 50), ylim=(-50, 50))
line, = axes.plot([], [], lw=2)

plt.title('Growing Coil')
plt.axis('off')

xdata, ydata = [], []


def init():
    line.set_data([], [])
    return line,


def animate(i):
    t = i * 0.1
    x = t * np.sin(t)
    y = t * np.cos(t)

    xdata.append(x)
    ydata.append(y)

    line.set_data(xdata, ydata)

    return line,


ani = animation.FuncAnimation(figure, animate, init_func=init,
                              frames=500, interval=20, repeat=False)

plt.show()

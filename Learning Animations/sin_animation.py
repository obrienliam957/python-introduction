import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


# Setting the limits of the axes
def setupAxes(axes):
    axes.set_xlim(xmin=0, xmax=2 * np.pi)
    axes.set_ylim(ymin=-2, ymax=2)


figure = plt.figure()

figure.suptitle("Plotting animated sin diagrams")

# Adding subplot and defining the way the lines are printed
axesLine = figure.add_subplot(211)
setupAxes(axesLine)
# plotLine, says to assign the variable line axes.plot(...)[0]
plotLine, = plt.plot([], [], lw=3)

axesMarker = figure.add_subplot(212)
setupAxes(axesMarker)
axesMarker.set_xlabel("theta")
axesMarker.set_ylabel("sin(theta)")
plotMarker, = plt.plot([], [], marker='o', markersize=15.0)

# Adjusting subplots so the labels are readable
plt.subplots_adjust(hspace=0.5)

lines = [plotLine, plotMarker]
NumberOfPoints = 100


# Initialise the line data to be empty arrays
def init():
    lines[0].set_data([], [])
    lines[1].set_data([], [])
    return lines


# Accept frame i
# In this case it returns data points for the whole chart
# between the limit x = 0 -> 4
def animateLine(i):
    phaseShift = (i * 2 * np.pi / NumberOfPoints) % 2 * np.pi
    x = np.linspace(0, 2 * np.pi, 1000)
    y = np.sin(2 * np.pi * x - phaseShift)
    return [x, y]


# Returns the position of theta and sin(theta)
def animateMarker(i):
    theta = (i * 2 * np.pi / NumberOfPoints) % 2 * np.pi
    x = theta
    y = np.sin(theta)
    return [x, y]


# Returns new line data to animate
def animate(i):
    lines[0].set_data(animateLine(i))
    lines[1].set_data(animateMarker(i))

    return lines


# Calling matplotlibs animation function passing the figure to plot to,
# the animation function which returns the datapoints to animate
# init_func: the initialising function defining points on the chart
# frames: total frames to animate for
# interval: delay between frames in milliseconds
# blit: optimize drawing by only redrawing updated points
graphicalAnimationLine = animation.FuncAnimation(figure, animate,
                                                 frames=500, interval=20, blit=True)

plt.show()
